/*
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 * Thanks to Lewis Lepton for its youtube tutorial serie: <https://www.youtube.com/playlist?list=PL4neAtv21WOmmR5mKb7TQvEQHpMh1h0po>
 * Thanks to by James Hofmann for its Tutorial for Kha Framework: <https://github.com/RblSb/khaguide>
 *
 */
/**
 * Classe pour une TileMap
 */
class Tilemap<T> {
  /** largeur **/
  public var w:Int;

  /** Hauteur **/
  public var h:Int;

  /** largeur de tile **/
  public var tw:Int;

  /** Hauteur de tile **/
  public var th:Int;

  /** Contenu de la map */
  public var d:Array<T>;

  /**
   * Créer une TileMap
   * @param w largeur
   * @param h Hauteur
   * @param tw largeur de tile
   * @param th Hauteur de tile
   * @param v valeur d'initialisation de chaque cellule
   */
  public function new(w, h, tw, th, v) {
    this.w = w;
    this.h = h;
    this.tw = tw;
    this.th = th;
    this.d = [for (i in 0...w * h) v];
  }

  /**
   * Renvoyer la coordonnée en X en fonction d'un index
   * @param idx
   * @return Int
   */
  public inline function x(idx:Int):Int {
    return idx % w;
  }

  /**
   * Renvoyer la coordonnée en Y en fonction d'un index
   * @param idx
   * @return Int
   */
  public inline function y(idx:Int):Int {
    return Std.int(idx / w);
  }

  /**
   * Renvoyer l'index en fonction des coordonnée en X et Y
   * @param x
   * @return Float
   */
  public inline function i(x:Int, y:Int):Int {
    if (x < 0) return -1;
    else if (x >= w) return -1;
    else if (y < 0) return -1;
    else if (y >= h) return -1;
    else return y * w + x;
  }

  /**
   * Convertir une coordonnée en X de tile vers pixel
   * @param x
   * @return Float
   */
  public inline function x2p(x:Float):Float {
    return x * tw;
  }

  /**
   * Convertir une coordonnée en Y de tile vers pixel
   * @param y
   * @return Float
   */
  public inline function y2p(y:Float):Float {
    return y * th;
  }

  /**
   * Convertir une coordonnée en X de pixel vers tile
   * @param p
   * @return Float
   */
  public inline function p2x(p:Float):Float {
    return p / tw;
  }

  /**
   * Convertir une coordonnée en Y de pixel vers tile
   * @param p
   * @return Float
   */
  public inline function p2y(p:Float):Float {
    return p / th;
  }
  /**
   * Convertir des coordonnées en X,Y vers un index
   * @param x
   * @param y
   * @return Int
   */
  public inline function p2i(x:Float, y:Float):Int {
    var tx = Std.int(x / tw);
    var ty = Std.int(y / th);
    return i(tx, ty);
  }
}