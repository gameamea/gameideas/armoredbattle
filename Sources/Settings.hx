import kha.Color;

class Settings {
  /** Titre du jeu */
  public static inline var GAME_TITLE = "Armored Battle";

  /** largeur de la fenêtre **/
  public static inline var WINDOW_WIDTH = 1280;

  /** Hauteur de la fenêtre **/
  public static inline var WINDOW_HEIGHT = 720;

  /** largeur de la zone de jeu. Si différente de celle  de la fenêtre, une échelle sera automatiquement appliquée **/
  public static inline var GAME_WIDTH:Int = Std.int(WINDOW_WIDTH / 2);

  /** Hauteur de la zone de jeu. Si différente de celle  de la fenêtre, une échelle sera automatiquement appliquée **/
  public static inline var GAME_HEIGHT:Int = Std.int(WINDOW_HEIGHT / 2);

  /** frames per second (refresh rate) **/
  public static inline var FPS = 60;

  /** Couleur de fond */
  public static inline var BG_COLOR = Color.Blue;

  /** Couleur de texte */
  public static inline var FG_COLOR = Color.White;
}