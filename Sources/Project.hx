/*
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 * Thanks to Lewis Lepton for its youtube tutorial serie: <https://www.youtube.com/playlist?list=PL4neAtv21WOmmR5mKb7TQvEQHpMh1h0po>
 * Thanks to by James Hofmann for its Tutorial for Kha Framework: <https://github.com/RblSb/khaguide>
 *
 */
import kha.Framebuffer;
import kha.Image;
import kha.Scaler;
import kha.System;

class Project {
  var backbuffer:Image;

  public function new(windowWidth:Int, windowHeight:Int) {
    // create a buffer to draw to
    backbuffer = Image.createRenderTarget(windowWidth, windowHeight);
  }

  public function update() {}

  public function render(framebuffer:Framebuffer) {
    var g = backbuffer.g2;
    // clear our backbuffer using graphics2
    g.begin(Settings.BG_COLOR);
    g.end();
    // draw our backbuffer onto the active framebuffer
    framebuffer.g2.begin();
    Scaler.scale(backbuffer, framebuffer, System.screenRotation);
    framebuffer.g2.end();
  }
}