/*
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/**
 * Thanks to Lewis Lepton for its youtube tutorial serie: <https://www.youtube.com/playlist?list=PL4neAtv21WOmmR5mKb7TQvEQHpMh1h0po>
 * Thanks to by James Hofmann for its Tutorial for Kha Framework: <https://github.com/RblSb/khaguide>
 *
 */
let project = new Project("New Project");
project.addCDefine("DEBUG_LEVEL=1");

project.addAssets("Assets/GFX", {
  nameBaseDir: "GFX",
  destination: "{dir}/{name}",
  name: "{dir}/{name}"
});
project.addAssets("Assets/Fonts", {
  nameBaseDir: "Font",
  destination: "{name}",
  name: "{name}"
  /* toutes les options possibles (selon l'analyse du code source de khamake )
  nameBaseDir: "GFX",
  destination: "{dir}/{name}",
  name: "{dir}/{name}"
  namePathSeparator: "/",
  md5sum: true,
  noprocessing: true,
  notinlist: {},
  from: ??,
  parallelAssetConversion : 0,
  target: "linux"
  */
});

project.addSources("Sources");

resolve(project);
