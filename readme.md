# ArmoredBattle

## Description

Il s'agit de reprendre une partie de l'univers et du gameplay de World of Tank et de l'appliquer à un environnement 2D.
Le projet final est assez ambitieux (adapter les map existantes du jeu original, jouer en équipe avec des alliés PNJ, proposer plusieurs choix de véhicules, gérer des upgrade...).

===

## Mouvements et actions du joueur

- **TODO**


## Interactions

- **TODO**

## Copyrights

Ecrit en Haxe et en utilisant le Framework Kha.
Développé en utilisant principalement Kode Studio.

---

(C) 2019 GameaMea <http://www.gameamea.com>

